#ifndef RASTERIZE_H
#define RASTERIZE_H

#include"geometry.h"
#include"renderTarget.h"

#include<vector>

/*
double SuperSample(const BBox& bbox, const Segment& seg, unsigned int sampleLevel);

void RasterizeTriangle(const Triangle& tri, RenderTarget& rt, unsigned int sampleLevel);

void RasterizeTriangleList(const std::vector<Triangle>& tri, RenderTarget& rt, unsigned int sampleLevel);


void RasterizeTriangleListRaw(const float* pVertices, unsigned int verticesCount, const unsigned short* pIndices, unsigned int indicesCount, double* pRenderTarget, unsigned int resx, unsigned int resy, const double* pBoundaries, unsigned int sampleLevel);
*/
void RasterizePolygon(const std::vector<Vector>& points, double amplitude, RenderTarget& rt);

void RasterizePolygonRaw(const float* pPoints, unsigned int pointCount, unsigned int polySize, double amplitude, double* pRt, unsigned int resx, unsigned int resy);

void RasterizePolygonBatch(const std::vector<Vector>& points, const std::vector<unsigned int>& polygonSections, const std::vector<double>& amplitude, RenderTarget& rt);

void RasterizePolygonBatchRaw(const float* pPoints, unsigned int pointCount, const unsigned short* pPolygonSections, const double* pAmplitude, unsigned int polygonCount, double* pRt, unsigned int resx, unsigned int resy);

// Test of the algorithm (in C++) that will run on the GPU
void RasterizePolygonBatch2(const std::vector<Vector>& points, const std::vector<unsigned int>& polygonSections, const std::vector<double>& amplitude, RenderTarget& rt);

void RasterizePolygonBatchRaw2(const float* pPoints, unsigned int pointCount, const unsigned short* pPolygonSections, const double* pAmplitude, unsigned int polygonCount, double* pRt, unsigned int resx, unsigned int resy);

// Offload computations on a GPU using OpenCL
void RasterizePolygonBatchRawGPU(const float* pPoints, unsigned int pointsCount, const unsigned short* polygonSections, const double* pAmplitude, unsigned int polygonCount, double* pRt, unsigned int resx, unsigned int resy);

#endif // RASTERIZE_H

