import numpy as np
import matplotlib.pyplot as plt
import rasterize_module as rast

rt = np.zeros([100, 100])
"""
vertices = np.array([[-0.1, 0.5, 0.8, 0.9, 0.1, -0.05],[-0.1, 0.8, 0.4, 0.9, 0.9, 0.4]])
#indices = np.array([0, 1, 2, 1, 3, 2, 3, 1, 4, 5, 4, 1], dtype=np.uint32)
indices = np.array([0, 1, 2], dtype=np.uint32)
boundaries = np.array([-1, 1, 1, -1], dtype=np.double)

rast.RasterizeTriangleListPy(vertices, indices, rt, boundaries, 4)
"""

points = np.array([[5., 20., 90.], [5., 80., 50.]], dtype=np.float32)
sections = np.array([3], dtype=np.uint16)

print(points.shape)

rast.RasterizePolygonBatchGPUPy(points, sections,  np.array([1.0]), rt)

plt.imshow(rt, cmap='Greys_r')
plt.show()

