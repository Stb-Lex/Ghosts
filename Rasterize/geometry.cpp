#include"geometry.h"


bool BBox::Intersect(const Segment& seg) const
{
  Segment* bboxSeg = GetSegments();


  for(unsigned int i = 0; i < 4; i++)
  {
    Vector v;
    if(!bboxSeg[i].Intersect(seg, v))
      continue;
    
    if(v.x >= leftBottom.x && v.x <= rightTop.x && v.y <= leftBottom.y && v.y >= rightTop.y)
    {
      delete[] bboxSeg;
      return true;
    }
  }

  delete[] bboxSeg;
  return false;
}

