# distutils: language = c++
# distutils: sources = rasterize.cpp renderTarget.cpp geometry.cpp rasterize_gpu.cpp


cimport numpy as np
import numpy as np

cdef extern from "rasterize.h":
    void RasterizePolygonBatchRaw(const float* pPoints, unsigned int pointCount, const unsigned short* pPolygonSections, const double* pAmplitude, unsigned int polygonCount, double* pRt, unsigned int resx, unsigned int resy);
    void RasterizePolygonBatchRawGPU(const float* pPoints, unsigned int pointCount, const unsigned short* pPolygonSections, const double* pAmplitude, unsigned int polygonCount, double* pRt, unsigned int resx, unsigned int resy);

def RasterizePolygonBatchPy(np.ndarray[np.float32_t, ndim=2] points, np.ndarray[np.uint16_t, ndim=1] polygonSections, np.ndarray[np.double_t, ndim=1] amplitude, np.ndarray[np.double_t, ndim=2] renderTarget):
    RasterizePolygonBatchRaw(<const float*> points.data, points.shape[1], <const unsigned short*> polygonSections.data, <const double*> amplitude.data, polygonSections.size, <double*> renderTarget.data, renderTarget.shape[0], renderTarget.shape[1])


def RasterizePolygonBatchGPUPy(np.ndarray[np.float32_t, ndim=2] points, np.ndarray[np.uint16_t, ndim=1] polygonSections, np.ndarray[np.double_t, ndim=1] amplitude, np.ndarray[np.double_t, ndim=2] renderTarget):
    pointSize = points.shape[1]
    points = points.T.flatten().reshape(2, pointSize)
    RasterizePolygonBatchRawGPU(<const float*> points.data, points.shape[1], <const unsigned short*> polygonSections.data, <const double*> amplitude.data, polygonSections.size, <double*> renderTarget.data, renderTarget.shape[0], renderTarget.shape[1])

