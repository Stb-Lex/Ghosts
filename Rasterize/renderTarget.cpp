#include "renderTarget.h"

#include<fstream>
#include<cstring>

RenderTarget::RenderTarget(unsigned int _resx, unsigned int _resy, const BBox& _boundaries)
{
  resx = _resx;
  resy = _resy;
  boundaries = _boundaries;
  
  pBuf = new double[resx*resy];

  Fill(0.0);

  ComputeTransformation();
}


RenderTarget::~RenderTarget()
{
  //  delete[] pBuf;
}


void RenderTarget::Release()
{
  delete[] pBuf;
}


void RenderTarget::Dump(const char* filename) const
{
  std::ofstream out;
  out.open(filename, std::ofstream::out);

  for(unsigned int i = 0; i < resy; i++)
  {
    out << pBuf[i*resx];
    for(unsigned int j = 1; j < resx; j++)
      out << "," << pBuf[i*resx+j];

    out << std::endl;
  }

  out.close();
}


void RenderTarget::Fill(double val)
{
  if(val == 0.0)
    memset(pBuf, 0, resx*resy*sizeof(double));
  else
    for(unsigned int i = 0; i < resy; i++)
      for(unsigned int j = 0; j < resx; j++)
        pBuf[i*resx+j] = val;
}


void RenderTarget::ComputeTransformation(void)
{
  translate = Vector(-boundaries.leftBottom.x, -boundaries.rightTop.y);
  scale = Vector(resx/(boundaries.rightTop.x - boundaries.leftBottom.x), resy/(boundaries.leftBottom.y - boundaries.rightTop.y));
}

