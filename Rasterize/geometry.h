
#ifndef GEOMETRY_H
#define GEOMETRY_H

#include<cmath>
#include<vector>
#include<iostream>

using namespace std;

const double epsilon = 0.001;

// Minimal vector class
// There is no distinction between points and vector
struct Vector
{
  Vector(void): x(0.0), y(0.0) {}
  Vector(float _x, float _y) : x(_x), y(_y) {}

  Vector operator+(const Vector& v) const;
  Vector operator-(const Vector& v) const;
  Vector operator*(const Vector& v) const; // Scale
  Vector operator/(const Vector& v) const;
  Vector operator*(float scalar) const;
  Vector operator/(float scalar) const;

  Vector& operator+=(const Vector& v);
  Vector& operator-=(const Vector& v);
  Vector& operator*=(const Vector& v);
  Vector& operator/=(const Vector& v);
  Vector& operator*=(float scalar);
  Vector& operator/=(float scalar);

  float Dot(const Vector& v) const;

  float x, y;
};


// A segment class defined by a position and a direction
struct Segment
{
  Segment(void) : pos(Vector()), direction(Vector()) {}
  Segment(const Vector& _pos, const Vector& _direction) : pos(_pos), direction(_direction) {}

  void BuildFromTwoPositions(const Vector& pos1, const Vector& pos2);

  Vector GetPoint(float t) const;

  bool Intersect(const Segment& seg, Vector& v) const;
  int Side(const Vector& point) const;
  
  Vector pos;
  Vector direction;
};


// A triangle class defined by 3 distinct points defined in ccw order
struct Triangle
{
  Triangle() : p1(Vector()), p2(Vector()), p3(Vector()) {}
  Triangle(const Vector& _p1, const Vector& _p2, const Vector& _p3) : p1(_p1), p2(_p2), p3(_p3) {}  

  Segment* GetSegments() const;

  bool IsInside(const Vector& point) const;

  Vector p1, p2, p3;
};


// Bounding box class
struct BBox
{
  BBox() : leftBottom(Vector()), rightTop(Vector()) {}
  BBox(const Vector& _leftBottom, const Vector& _rightTop) : leftBottom(_leftBottom), rightTop(_rightTop) {}
  BBox(const Triangle& tri);
  BBox(const vector<Vector>& points);

  Segment* GetSegments(void) const;

  bool IsInside(const Vector& point) const;
  bool Intersect(const Triangle& tri) const;
  bool IntersectTriangle(const Segment* seg) const;
  bool Intersect(const Segment& seg) const;

  Vector leftBottom, rightTop;
};

// Branchless sign function
float sgn(float val);

// A standard and boring implementation of the clamp function (since it is not provided by the STL)
float clamp(float val, float lower, float upper);

// Test whether a triangle intersect with a bounding box or not
// It only test intersection, i.e. it will return false if one of the element is contained in the other
bool IntersectTriangle(const BBox& bbox, const Segment* pTrigSeg);

// Test whether a point is inside a ccw triangle defined by its segment (used as an optimisation: no memory allocation is necessary)
bool IsInsideTriangle(const Segment* pSeg, const Vector& point);

// Translate and scale a triangle
Triangle TransformTriangle(const Triangle& tri, const Vector& translate, const Vector& scale);

// Gives the winding number of a point inside a polygon
int PolyWind(const std::vector<Vector>& poly, const Vector& point);
int PolyWindRaw(const float* pPoly, unsigned int pointsCount, unsigned int polySize, const Vector& points);

inline Vector Vector::operator+(const Vector& v) const
{
  return Vector(x + v.x, y + v.y);
}


inline Vector Vector::operator-(const Vector& v) const
{
  return Vector(x - v.x, y - v.y);
}


inline Vector Vector::operator*(const Vector& v) const
{
  return Vector(x*v.x, y*v.y);
}


inline Vector Vector::operator/(const Vector& v) const
{
  return Vector(x/v.x, y/v.y);
}


inline Vector Vector::operator*(float scalar) const
{
  return Vector(scalar*x, scalar*y);
}


inline Vector Vector::operator/(float scalar) const
{
  return Vector(x/scalar, y/scalar);
}


inline Vector& Vector::operator+=(const Vector& v)
{
  x += v.x;
  y += v.y;

  return *this;
}


inline Vector& Vector::operator-=(const Vector& v)
{
  x -= v.x;
  y -= v.y;

  return *this;
}


inline Vector& Vector::operator*=(const Vector& v)
{
  x *= v.x;
  y *= v.y;

  return *this;
}


inline Vector& Vector::operator/=(const Vector& v)
{
  x /= v.x;
  y /= v.y;

  return *this;
}


inline Vector& Vector::operator*=(float scalar)
{
  x *= scalar;
  y *= scalar;

  return *this;
}


inline Vector& Vector::operator/=(float scalar)
{
  x /= scalar;
  y /= scalar;

  return *this;
}


inline float Vector::Dot(const Vector& v) const
{
  return x*v.x + y*v.y;
}


inline void Segment::BuildFromTwoPositions(const Vector& pos1, const Vector& pos2)
{
  pos = pos1;
  direction = pos2 - pos1;
}


inline Vector Segment::GetPoint(float t) const
{
  return pos + direction*t;
}


inline bool Segment::Intersect(const Segment& seg, Vector& v) const
{
  if(direction.x != 0 && direction.y != 0)
  {
    float a = seg.direction.x/direction.x;
    float b = seg.direction.y/direction.y;

    if(a == b)
      return false;

    float c = (pos.x - seg.pos.x)/direction.x + (seg.pos.y - pos.y)/direction.y;

    v = seg.GetPoint(c / (a-b));
    return true;
  }
  else if(direction.x != 0 && direction.y == 0.0)
  {
    // If both lines are parallele
    if(seg.direction.y == 0.0)
      return pos.y == seg.pos.y;

    v = seg.GetPoint((pos.y - seg.pos.y)/seg.direction.y);
    return true;
  }
  else if(direction.x == 0.0 && direction.y != 0.0)
  {
    // If both lines are parallele
    if(seg.direction.x == 0.0)
      return pos.y == seg.pos.y;

    v = seg.GetPoint((pos.x - seg.pos.x)/seg.direction.x);
    return true;
  }

  return false;
}


inline int Segment::Side(const Vector& point) const
{
  Vector pos2 = pos + direction;
  return sgn((pos2.x - pos.x)*(point.y - pos.y) - (pos2.y - pos.y)*(point.x - pos.x));
}


inline Segment* Triangle::GetSegments() const
{
  Segment* seg = new Segment[3];
  seg[0] = Segment(p1, p2 - p1);
  seg[1] = Segment(p2, p3 - p2);
  seg[2] = Segment(p3, p1 - p3);

  return seg;
}


inline bool Triangle::IsInside(const Vector& point) const
{
  Segment* seg = GetSegments();
  bool result = IsInsideTriangle(seg, point);
  delete[] seg;
  return result;
}


inline BBox::BBox(const Triangle& tri)
{
  leftBottom.x = fmin(tri.p1.x, fmin(tri.p2.x, tri.p3.x));
  leftBottom.y = fmin(tri.p1.y, fmin(tri.p2.y, tri.p3.y));
  rightTop.x = fmax(tri.p1.x, fmax(tri.p2.x, tri.p3.x));
  rightTop.y = fmax(tri.p1.y, fmax(tri.p2.y, tri.p3.y));
}


inline BBox::BBox(const std::vector<Vector>& points) : leftBottom(Vector()), rightTop(Vector())
{
  if(points.size() == 0)
    return;
  else if(points.size() == 1)
  {
    leftBottom = rightTop = Vector(points[0]);
  }
  else
  {
    
    
    for(unsigned int i = 2; i < points.size(); i++)
    {
      if(points[i].x < rightTop.x) rightTop.x = points[i].x;
      if(points[i].x > leftBottom.x) leftBottom.x = points[i].x;
      if(points[i].y > rightTop.y) rightTop.y = points[i].y;
      if(points[i].y < leftBottom.y) leftBottom.y = points[i].y;
    }

  }
}


inline Segment* BBox::GetSegments(void) const
{
  Segment* seg = new Segment[4];
  seg[0] = Segment(leftBottom, Vector(0, rightTop.y - leftBottom.y));
  seg[1] = Segment(Vector(leftBottom.x, rightTop.y), Vector(rightTop.x - leftBottom.x, 0));
  seg[2] = Segment(rightTop, Vector(0, leftBottom.y-rightTop.y));
  seg[3] = Segment(Vector(rightTop.x, leftBottom.y), Vector(leftBottom.x - rightTop.x, 0));

  return seg;
}


inline bool BBox::IsInside(const Vector& point) const
{
  return (point.x <= rightTop.x && point.x >= leftBottom.x && point.y <= rightTop.y && point.y >= leftBottom.y);
}


inline bool BBox::IntersectTriangle(const Segment* pTrigSeg) const
{
  for(unsigned int i = 0; i < 3; i++)
    if(Intersect(pTrigSeg[i]))
      return true;

  return false;
}


inline float sgn(float val)
{
  return ((0 < val) - (val < 0));
}


inline float clamp(float val, float lower, float upper)
{
  if(val < lower)
    val = lower;
  if(val > upper)
    val = upper;

  return val;
}


inline bool IsInsideTriangle(const Segment* pSeg, const Vector& point)
{
  return ((pSeg[0].Side(point) == -1) && (pSeg[1].Side(point) == -1) && (pSeg[2].Side(point) == -1));
}


inline Triangle TransformTriangle(const Triangle& tri, const Vector& translate, const Vector& scale)
{
  return Triangle((tri.p1 + translate)*scale, (tri.p2 + translate)*scale, (tri.p3 + translate)*scale);
}

inline int PolyWind(const std::vector<Vector>& poly, const Vector& point)
{
  int wind = 0;

  Vector pt = point;
  Vector p = poly.back();

  for(unsigned int i = 0; i< poly.size(); i++)
  {
    Vector d = poly[i];
    if(p.y <= pt.y)
    {
      if(d.y > pt.y && (p.x-pt.x)*(d.y-pt.y) - (p.y-pt.y)*(d.x-pt.x) > 0)
        wind++;
    }
    else
    {
      if(d.y <= pt.y && (p.x-pt.x)*(d.y-pt.y) - (p.y-pt.y)*(d.x-pt.x) < 0)
        wind--;
    }

    p = d;
  }

  return wind;
}


inline int PolyWindRaw(const float* pPoints, unsigned int pointCount, unsigned int polySize, const Vector& point)
{
  int wind = 0;

  Vector pt = point;
  Vector p(pPoints[polySize-1], pPoints[pointCount+polySize-1]);

  for(unsigned int i = 0; i< polySize; i++)
  {
    Vector d(pPoints[i], pPoints[i+pointCount]);
    if(p.y <= pt.y)
    {
      if(d.y > pt.y && (p.x-pt.x)*(d.y-pt.y) - (p.y-pt.y)*(d.x-pt.x) > 0)
        wind++;
    }
    else
    {
      if(d.y <= pt.y && (p.x-pt.x)*(d.y-pt.y) - (p.y-pt.y)*(d.x-pt.x) < 0)
        wind--;
    }

    p = d;
  }

  return wind;
}


#endif // GEOMETRY_H

