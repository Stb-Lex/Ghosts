int PolyWind(__global float2* points, const uint pointCount, const uint polySize, const float2 point)
{
  int wind = 0;
  float2 pt = point;
  float2 p = points[polySize-1];

  for(uint i = 0; i < polySize; i++)
  {
    float2 d = points[i];
    if(p.y <= pt.y)
    {
      if(d.y > pt.y && (p.x-pt.x)*(d.y-pt.y) - (p.y-pt.y)*(d.x-pt.x) > 0)
      {
        wind++;
      }
    }
    else
    {
      if(d.y <= pt.y && (p.x-pt.x)*(d.y-pt.y) - (p.y-pt.y)*(d.x-pt.x) < 0)
      {
        wind--;
      }
    }
    p = d;
  }

  return wind;
}

__kernel void rasterize(__global float2* points, const uint pointCount, __global ushort* polygonSections, __global double* amplitude, const uint polygonCount, __global double* rt, const uint resx)
{
  size_t idx = get_global_id(0);
  size_t idy = get_global_id(1);
  
  float2 curPoint = (float2)((float)idx + 0.5, (float)idy + 0.5);
  uint curIndex = 0;

  rt[idy*resx+idx] = 0;
  
  for(uint i = 0; i < polygonCount; i++)
  {
    if(abs(PolyWind(&points[curIndex], pointCount, polygonSections[i], curPoint)) == 1)
    {
      rt[idy*resx+idx] += amplitude[i];
    }
    curIndex += polygonSections[i];
  }
  
}

