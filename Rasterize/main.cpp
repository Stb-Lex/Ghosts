#include"renderTarget.h"
#include"rasterize.h"

int main(int argc, char** argv)
{
  /*
  RenderTarget rt = RenderTarget(512, 512, BBox(Vector(-0.13, 0.13), Vector(0.13, -0.13)));

  double vertices[] = {- 0.8, 0.10, 0.80, 0.9, -0.8, 0.50, 0.4, 0.9 };
  unsigned int indices[] = { 0, 1, 2, 1, 3, 2 };
  unsigned int res[] = { 512, 512 };
  double boundaries[] = {-1, -1, 1.0, 1.0};

  RasterizeTriangleListRaw(vertices, 4, indices, 6, rt.pBuf, res[0], res[1], boundaries, 4);
*/
  RenderTarget rt = RenderTarget(512, 512, BBox());
  
  float points[] = {10., 40., 100., 200., 150., 30., 200., 200., 300., 10., 50., 100., 500., 50., 10., 400., 100., 430.};
  unsigned short polygonSections[] = {6, 3};
  double amplitude[] = {1., 0.7};

  RasterizePolygonBatchRawGPU(points, 9, polygonSections, amplitude, 2, rt.pBuf, rt.resx, rt.resy);

  /*
  std::vector<Vector> points;
  points.push_back(Vector(1., 1.));
  points.push_back(Vector(90., 1.));
  points.push_back(Vector(90., 120.));
  points.push_back(Vector(1., 90.));

  points.push_back(Vector(100., 100.));
  points.push_back(Vector(400., 100.));
  points.push_back(Vector(200., 200.));
  points.push_back(Vector(100., 400.));

  RenderTarget rt = RenderTarget(1024, 1024, BBox());

  std::vector<unsigned int> polygonSections = {4, 8};
  std::vector<double> amplitude = {1., 0.5};

  RasterizePolygonBatch2(points, polygonSections, amplitude, rt);
  */  
  rt.Dump("out.csv");

  return 0;
}

