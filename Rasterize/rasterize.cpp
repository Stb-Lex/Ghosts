#include"rasterize.h"

#include<iostream>
#include<cstdlib>

using namespace std;

/*
double SuperSample(const BBox& bbox, const Segment* seg, unsigned int sampleLevel)
{
  double dx = (bbox.rightTop.x - bbox.leftBottom.x)/(double)sampleLevel;
  double dy = (bbox.leftBottom.y - bbox.rightTop.y)/(double)sampleLevel;

  Vector t = Vector(dx/2.0, dy/2.0);

  double lum = 0.0;
  double dlum = 1.0/(double)sampleLevel/(double)sampleLevel;

  for(unsigned int i = 0; i < sampleLevel; i++)
  {
    for(unsigned int j = 0; j < sampleLevel; j++)
    {
      Vector point(bbox.leftBottom.x + i * dx + t.x, bbox.rightTop.y + j * dy + t.y);
      if(IsInsideTriangle(seg, point))
        lum += dlum;
    }
  }

  return lum;
}


void RasterizeTriangle(const Triangle& tri, RenderTarget& rt, unsigned int sampleLevel)
{
  Triangle projTri = TransformTriangle(tri, rt.translate, rt.scale);
  BBox bbox = BBox(projTri);

  // Rasterize the triangle
  int xmin = clamp(int(floor(bbox.leftBottom.x)), 0, rt.resx);
  int ymin = clamp(int(floor(bbox.leftBottom.y)), 0, rt.resy);
  int xmax = clamp(int(ceil(bbox.rightTop.x)), 0, rt.resx);
  int ymax = clamp(int(ceil(bbox.rightTop.y)), 0, rt.resy);

  Segment* seg = projTri.GetSegments();

  for(int y = ymin; y < ymax; y++)
  {
    for(int x = xmin; x < xmax; x++)
    {
      BBox pixelBBox = BBox(Vector(x, y + 1), Vector(x + 1, y));
      Vector point((double)x + 0.5, (double)y + 0.5);
      
      if(pixelBBox.IntersectTriangle(seg))
        rt.pBuf[y*rt.resx+x] = std::min(rt.pBuf[y*rt.resx+x] + SuperSample(pixelBBox, seg, sampleLevel), 1.0);
      else if(IsInsideTriangle(seg, point))
        rt.pBuf[y*rt.resx+x] = 1.0;
    }
  }

  delete[] seg;
}


void RasterizeTriangleList(const std::vector<Triangle>& tri, RenderTarget& rt, unsigned int sampleLevel)
{
  for(unsigned int i = 0; i < tri.size(); i++)
    RasterizeTriangle(tri[i], rt, sampleLevel);
}


void RasterizeTriangleListRaw(const double* pVertices, unsigned int verticesCount, const unsigned int* pIndices, unsigned int indicesCount, double* pRenderTarget, unsigned int resx, unsigned int resy, const double* pBoundaries, unsigned int sampleLevel)
{
  RenderTarget rt(resx, resy, BBox(Vector(pBoundaries[0], pBoundaries[3]), Vector(pBoundaries[2], pBoundaries[1])), pRenderTarget);
  for(unsigned int i = 0; i < indicesCount; i += 3)
    RasterizeTriangle(Triangle(Vector(pVertices[pIndices[i]], pVertices[pIndices[i]+verticesCount]), Vector(pVertices[pIndices[i+1]], pVertices[pIndices[i+1]+verticesCount]), Vector(pVertices[pIndices[i+2]], pVertices[pIndices[i+2]+verticesCount])), rt, sampleLevel);
}
*/

void RasterizePolygon(const std::vector<Vector>& points, double amplitude, RenderTarget& rt)
{
  BBox bbox(Vector(0, 0), Vector(rt.resx, rt.resy));
  
  int xmin = clamp(int(floor(bbox.leftBottom.x)), 0, rt.resx);
  int ymin = clamp(int(floor(bbox.leftBottom.y)), 0, rt.resy);
  int xmax = clamp(int(ceil(bbox.rightTop.x)), 0, rt.resx);
  int ymax = clamp(int(ceil(bbox.rightTop.y)), 0, rt.resy);

  for(int y = ymin; y < ymax; y++)
  {
    for(int x = xmin; x < xmax; x++)
    {
      Vector point((float)x + 0.5, (float)y + 0.5);
      if(abs(PolyWind(points, point)) == 1)
        rt.pBuf[y*rt.resx+x] += amplitude;
    }
  }
}


void RasterizePolygonRaw(const float* pPoints, unsigned int pointCount, unsigned int polySize, double amplitude, double* pRt, unsigned int resx, unsigned int resy)
{
  for(int y = 0; y < resy; y++)
  {
    for(int x = 0; x < resx; x++)
    {
      Vector point((float)x + 0.5, (float)y + 0.5);
      if(abs(PolyWindRaw(pPoints, pointCount, polySize, point)) == 1)
        pRt[y*resx+x] += amplitude;
    }
  }
}


void RasterizePolygonBatch(const std::vector<Vector>& points, const std::vector<unsigned short>& polygonSections, const std::vector<double>& amplitude, RenderTarget& rt)
{
  RasterizePolygon(std::vector<Vector>(points.begin(), points.begin() + polygonSections[0]), amplitude[0], rt);
  
  for(unsigned int i = 1; i < polygonSections.size(); i++)
    RasterizePolygon(std::vector<Vector>(&points[polygonSections[i-1]], &points[polygonSections[i]]), amplitude[i], rt);
}


void RasterizePolygonBatchRaw(const float* pPoints, unsigned int pointCount, const unsigned short* pPolygonSections, const double* pAmplitude, unsigned int polygonCount, double* pRt, unsigned int resx, unsigned int resy)
{
  unsigned int curIndex = 0;
  for(unsigned int i = 0; i < polygonCount; i++)
  {
    RasterizePolygonRaw(&pPoints[curIndex], pointCount, pPolygonSections[i], pAmplitude[i], pRt, resx, resy);
    curIndex += pPolygonSections[i];
  }
}


void RasterizePolygonBatch2(const std::vector<Vector>& points, const std::vector<unsigned short>& polygonSections, const std::vector<double>& amplitude, RenderTarget& rt)
{
  for(unsigned int y = 0; y < rt.resy; y++)
  {
    for(unsigned int x = 0; x < rt.resx; x++)
    {
      Vector curPoint(x + 0.5, y + 0.5);
      
      if(abs(PolyWind(std::vector<Vector>(points.begin(), points.begin() + polygonSections[0]), curPoint)) == 1)
        rt.pBuf[y*rt.resy + x] += amplitude[0];
      
      for(unsigned int i = 1; i < polygonSections.size(); i++)
        if(abs(PolyWind(std::vector<Vector>(&points[polygonSections[i-1]], &points[polygonSections[i]]), curPoint)) == 1)
          rt.pBuf[y*rt.resy + x] += amplitude[i];
    }
  }
}

void RasterizePolygonBatchRaw2(const float* pPoints, unsigned int pointCount, const unsigned short* pPolygonSections, const double* pAmplitude, unsigned int polygonCount, double* pRt, unsigned int resx, unsigned int resy)
{
  for(unsigned int y = 0; y < resy; y++)
  {
    for(unsigned int x = 0; x < resx; x++)
    {
      Vector curPoint(x + 0.5, y + 0.5);
      unsigned int curIndex = 0;
      for(unsigned int i = 0; i < polygonCount; i++)
      {
        if(abs(PolyWindRaw(&pPoints[curIndex], pointCount, pPolygonSections[i], curPoint)) == 1)
          pRt[y*resx+x] += pAmplitude[i];
        
        curIndex += pPolygonSections[i];
      }
    }
  }
}

