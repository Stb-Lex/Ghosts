from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy

setup(
    name = "rasterize module",
    ext_modules = [
        Extension("rasterize_module",
                  sources=["rasterize_module.pyx", "rasterize.cpp", "renderTarget.cpp", "geometry.cpp", "rasterize_gpu.cpp"],
                  extra_compile_args=['-O2', '-std=c++11'],
                  library_dirs=['/usr/lib/'],
                  libraries=["OpenCL"],
                  language='c++'
                  )
        ],
    cmdclass = {'build_ext': build_ext}
    )

