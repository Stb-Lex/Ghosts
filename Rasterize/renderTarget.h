#ifndef RENDERTARGET_H
#define RENDERTARGET_H

#include"geometry.h"

struct RenderTarget
{
  RenderTarget(void) : pBuf(0), resx(0), resy(0) {}
  RenderTarget(unsigned int _resx, unsigned int _resy, const BBox& _boundaries);
RenderTarget(unsigned int _resx, unsigned int _resy, const BBox& _boundaries, double* _pBuf) : resx(_resx), resy(_resy), boundaries(_boundaries), pBuf(_pBuf) {ComputeTransformation();}
  ~RenderTarget(void);

  void Dump(const char* filename) const;
  void Fill(double val = 0.0);
  
  void ComputeTransformation(void); // Compute translations/scale vectors
  
  void Release();
  
  double* pBuf;
  unsigned int resx, resy;
  BBox boundaries;

  // Transformations used to pass from camera space to pixel space
  Vector translate;
  Vector scale;
};

#endif // RENDERTARGET_H

