import numpy as np
import matplotlib.pyplot as plt
import sys

if(len(sys.argv) == 1):
    print("Usage : draw_rt.py filename")
    exit()

filename = sys.argv[1]

rt = np.genfromtxt(filename, delimiter=",")

plt.imshow(rt, cmap='Greys_r', interpolation='none')
plt.show()

