cmake_minimum_required(VERSION 3.0)

project(Rasterize)

find_package(OpenCL REQUIRED)
include_directories(${OpenCL_INCLUDE_DIRS})
link_directories(${OpenCL_LIBRARY})

add_executable(Rasterize rasterize.cpp geometry.cpp main.cpp renderTarget.cpp rasterize_gpu.cpp)

target_compile_features(Rasterize PRIVATE cxx_generalized_initializers)
target_include_directories(Rasterize PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(Rasterize ${OpenCL_LIBRARY})

