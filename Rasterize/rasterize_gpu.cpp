#include"rasterize.h"
#include<CL/opencl.h>
#include<iostream>
#include<string>
#include<cstring>
#include<fstream>
#include<sstream>

using namespace std;

void RasterizePolygonBatchRawGPU(const float* pPoints, unsigned int pointCount, const unsigned short* pPolygonSections, const double* pAmplitude, unsigned int polygonCount, double* pRt, unsigned int resx, unsigned int resy)
{
  cout << "Rasterizing polygon batch on GPU" << endl;
  cl_platform_id platformId;
  cl_uint platformCount;
  cout << "Retrieving platform id... ";
  cl_int curError = clGetPlatformIDs(1, &platformId, &platformCount);

  if(curError != CL_SUCCESS)
  {
    cerr << endl << "Error when trying to retrieve platform ids !" << endl;
    cerr << "Error code: " << curError << endl;
    return;
  }

  cout << "Done" << endl;
  
  cl_device_id deviceId;
  cl_uint deviceCount;

  cout << "Getting devices... " << flush;
  curError = clGetDeviceIDs(platformId, CL_DEVICE_TYPE_GPU, 1, &deviceId, &deviceCount);

  if(curError != CL_SUCCESS)
  {
    cerr << "Error when trying to retrieve device ids !" << endl;
    cerr << "Error code: " << curError << endl;
    return;
  }

  cout << "Done" << endl;

  if(deviceCount == 0)
  {
    cerr << "Could not find a GPU ! Exiting..." << endl;
    return;
  }

  cl_context context;
  cl_context_properties properties[3];
  properties[0] = CL_CONTEXT_PLATFORM;
  properties[1] = (cl_context_properties)platformId;
  properties[2] = 0;

  cout << "Creating context... " << flush;
  context = clCreateContext(properties, 1, &deviceId, nullptr, nullptr, &curError);

  if(curError != CL_SUCCESS)
  {
    cerr << "Error when trying to create context !" << endl;
    cerr << "Error code: " << curError << endl;
    return;
  }

  cout << "Done" << endl;

  cout << "Creating command queue... " << flush;
  cl_command_queue commandQueue = clCreateCommandQueue(context, deviceId, 0, &curError);

  if(curError != CL_SUCCESS)
  {
    cerr << "Error when trying to create the command queue !" << endl;
    cerr << "Error code: " << curError << endl;
    return;
  }

  cout << "Done" << endl;

  cout << "Loading program file..." << flush;
  ifstream file("rasterize.cl");
  stringstream t;
  t << file.rdbuf();
  cout << "Done" << endl;

  char* tempSource = new char[t.str().size()];
  strcpy(tempSource, t.str().c_str());
  char** tempSource2 = &tempSource;

  cout << "Creating program... " << flush;
  cl_program program = clCreateProgramWithSource(context, 1, (const char**)tempSource2, nullptr, &curError);

  if(curError != CL_SUCCESS)
  {
    cerr << "Error when trying to create the program !" << endl;
    cerr << "Error code: " << curError << endl;
    return;
  }

  cout << "Done" << endl;

  cout << "Building program... " << flush;
  curError = clBuildProgram(program, 0, nullptr, nullptr, nullptr, nullptr);

  if(curError != CL_SUCCESS)
  {
    char buffer[8192];
    size_t length;
    cerr << "Could not build program !" << endl;
    clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_LOG, sizeof(buffer), &buffer, &length);

    cerr << string(buffer) << endl;
    return;
  }

  cout << "Done" << endl;

  cout << "Creating kernel... " << flush;
  cl_kernel kernel = clCreateKernel(program, "rasterize", &curError);

  if(curError != CL_SUCCESS)
  {
    cerr << "Error when trying to create the kernel !" << endl;
    cerr << "Error code: " << curError << endl;
    return;
  }

  cout << "Done" << endl;

  cout << "Creating device buffers... " << flush;
  cl_mem bufPoints = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float2)*pointCount, nullptr, nullptr);
  cl_mem bufPolygonSections = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_short)*polygonCount, nullptr, nullptr);
  cl_mem bufAmplitude = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_double)*polygonCount, nullptr, nullptr);
  cl_mem bufRenderTarget = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_double)*resx*resy, nullptr, nullptr);
  cout << "Done" << endl;

  cout << "Writing device buffers... " << flush;
  clEnqueueWriteBuffer(commandQueue, bufPoints, CL_TRUE, 0, sizeof(cl_float2)*pointCount, pPoints, 0, nullptr, nullptr);
  clEnqueueWriteBuffer(commandQueue, bufPolygonSections, CL_TRUE, 0, sizeof(cl_short)*polygonCount, pPolygonSections, 0, nullptr, nullptr);
  clEnqueueWriteBuffer(commandQueue, bufAmplitude, CL_TRUE, 0, sizeof(cl_double)*polygonCount, pAmplitude, 0, nullptr, nullptr);
  cout << "Done" << endl;

  clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufPoints);
  clSetKernelArg(kernel, 1, sizeof(cl_uint), &pointCount);
  clSetKernelArg(kernel, 2, sizeof(cl_mem), &bufPolygonSections);
  clSetKernelArg(kernel, 3, sizeof(cl_mem), &bufAmplitude);
  clSetKernelArg(kernel, 4, sizeof(cl_uint), &polygonCount);
  clSetKernelArg(kernel, 5, sizeof(cl_mem), &bufRenderTarget);
  clSetKernelArg(kernel, 6, sizeof(cl_uint), &resx);
  
  size_t dim[2];
  dim[0] = resx;
  dim[1] = resy;

  clEnqueueNDRangeKernel(commandQueue, kernel, 2, nullptr, dim, nullptr, 0, nullptr, nullptr);  

  cout << "Executing kernel... " << flush;
  clFinish(commandQueue);
  cout << "Done" << endl;
  
  cout << "Retrieving render target... " << flush;
  clEnqueueReadBuffer(commandQueue, bufRenderTarget, CL_TRUE, 0, sizeof(cl_double)*resx*resy, pRt, 0, nullptr, nullptr);
  cout << "Done" << endl;

  cout << "Cleaning..." << flush;
  clReleaseMemObject(bufPoints);
  clReleaseMemObject(bufPolygonSections);
  clReleaseMemObject(bufAmplitude);
  clReleaseMemObject(bufRenderTarget);
  clReleaseProgram(program);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(commandQueue);
  clReleaseContext(context);
  cout << "Done" << endl;
}

