#include<vector>
#include<string>
#include<iostream>
#include<cstdint>
#include<algorithm>
#include<fstream>
#include<iomanip>

using namespace std;


typedef char byte;
typedef unsigned int uint;


byte sum(const vector<byte>& in)
{
  byte result = 0;
  for(uint i = 0; i < in.size(); i++)
    result += in[i];

  return result;
}


struct path_t
{
  path_t(vector<byte> _path, double _T) : path(_path), T(_T) {}
  
  vector<byte> path;
  double T;
};


bool pathCompare(const path_t& i, const path_t& j)
{
  return i.T < j.T;
}


vector<path_t> computePath(uint numLayers, double threshold, const vector<double>& T, uint maxOrder, double& epsilon, double &Tfp, uint& count, const vector<path_t>& initPaths = vector<path_t>())
{
  vector<path_t> curPaths = vector<path_t>();
  vector<path_t> newPaths = vector<path_t>();
  vector<path_t> paths = vector<path_t>();

  bool cont = true;
  bool right; // Direction of propagation

  epsilon = 0.0;
  Tfp = 0.0;
  count = 1;
  if(initPaths.size())
    {
      curPaths = initPaths;
    }
  else
    {
      path_t curPath = path_t({1}, T[0]);
      curPaths.push_back(curPath);
      for(uint i = 2; i < numLayers; i++)
        {
          curPath.path[0] = i;
          curPath.T *= T[i];
          curPaths.push_back(curPath);
        }

      right = false; // Rays already have been propagated to the right

      // Straight transmission through the lens system
      curPath.path[0]++;
      curPath.T *= T[numLayers];
      paths.push_back(curPath);
      Tfp = curPath.T;
    }

  while(cont)
    {
      for(uint i = 0; i < curPaths.size(); i++)
        {
          byte range = 0; // The number of child paths we will visit
          byte incr = 1; // The incrementing constant. 1 if we go to the right, -1 to the left

          byte curLayer = sum(curPaths[i].path);
          double curT = curPaths[i].T*(1.0-T[curLayer]);
      
          if(right)
            range = numLayers - curLayer;
          else
            {
              range = curLayer;
              incr = -1;
            }

          for(uint j = 0; j < range; j++)
            {
              curLayer += incr;
              curT *= T[curLayer];

              if(curT <= threshold) // The transmission is below the limit, we know the next paths will also be below this limit, so we can skip them
                {
                  epsilon += curT;
                  break;
                }

              vector<byte> path = curPaths[i].path;
              path.push_back(incr*(j+1));

              if(curLayer == numLayers)
                {
                  paths.push_back(path_t(path, curT));
                  count++;
                  Tfp += curT;
                  break; // We skip the next paths since they will go past the focal plan
                }
              else
                {
                  //	  if(path.size() < (maxOrder + 1))
                  newPaths.push_back(path_t(path, curT));
                }
            }
        }

      curPaths = newPaths;
      newPaths.clear();

      right = !right;

      if(!curPaths.size())
        cont = false;

    }

  return paths;
}


string pathToString(const path_t& path)
{
  string str;
  for(uint i = 0; i < path.path.size(); i++)
    {
      if(i%2 == 0)
        {
          for(uint j = 0; j < path.path[i]; j++)
            str += "r";
        }
      else        
        {
          for(uint j = 0; j < -path.path[i]; j++)
            str += "l";
        }
    }

  return str;
}


void dumpToCsv(const vector<path_t>& paths, const string& filename)
{
  ofstream file(filename, ofstream::out);
  
  for(uint i = 0; i < paths.size(); i++)
    file << pathToString(paths[i]) << endl;

  file.close();
}


int main(int argc, char** argv)
{
  if(argc < 2)
  {
    cout << "Usage : ComputePath threshold outFilename" << endl;
    return 0;
  }
  double epsilon = 0.0;
  double Tfp = 0.0;
  uint count = 0;
  float threshold = atof(argv[1]);
  vector<double> T = {1, 0.98, 0.98, 0.98, 0.98, 0.98, 0.98, 0.98, 0.98, 0.98, 0.98, 0.8, 0.8, 0.98, 0.98, 0.7};

  vector<path_t> paths = computePath(15, threshold, T, 0, epsilon, Tfp, count);
  cout << count << " paths have been found" << endl;
  dumpToCsv(paths, argv[2]);

  return 0;
}
