import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("out.csv", delimiter=",")

print("Focus at dx=" + str(np.extract(data[:,1]==np.min(data[:,1]), data[:,0])))

plt.xlabel("$dz$")
plt.ylabel("std. avg.")
plt.plot(data[:,0], data[:,1])
plt.show()

