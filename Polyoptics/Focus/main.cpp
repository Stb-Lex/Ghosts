#include<Shared/megacam.h>

#include<Eigen/Dense>
#include<Eigen/Core>

#include<iostream>
#include<fstream>

using namespace std;

const static Eigen::IOFormat CSVFormat(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", "\n");

void writeToCSVFile(const char* name, const Eigen::MatrixXf& matrix)
{
  ofstream file(name);
  file << matrix.format(CSVFormat);
  file.close();
}

int main(int argc, char** argv)
{
  if(argc < 2)
  {
    cout << "Compute dx (focus parameter) in Megacam" << endl;
    cout << "Usage : Focus order" << endl;
    return 0;
  }

  int trunc = atoi(argv[1]);
  
  float minX = -0.05f;
  float maxX = 0.05f;
  const unsigned int vecSize = 1000;
  const unsigned int maxStep = 1000;

  Eigen::VectorXf space = Eigen::VectorXf::LinSpaced(maxStep, minX, maxX);
  Eigen::MatrixXf focusData = Eigen::MatrixXf::Zero(maxStep, 2);

  
  for(unsigned int i = 0; i < space.size(); i++)
  {
    LayerStack curMegacam = BuildMegacam(space(i), 500, trunc);
    OpticalPath curOpticalPath = curMegacam.ComputeOpticalPath("rrrrrrrrrrrrrrr");

    Eigen::VectorXf randomPupilPoints = Eigen::VectorXf::Random(vecSize);
    Eigen::VectorXf focalPoints(vecSize);

    for(unsigned int j = 0; j < vecSize; j++)
    {
      PhaseSpacePoint in(randomPupilPoints(j), randomPupilPoints(j), 0.f, 0.f);
      PhaseSpacePoint out = curOpticalPath.Evaluate(in);
      focalPoints(j) = out.px;
    }

    float mean = focalPoints.mean();
    float stddev = 0.f;

    for(unsigned int i = 0; i < vecSize; i++)
      stddev += (focalPoints(i) - mean)*(focalPoints(i) - mean);

    stddev /= vecSize;
    stddev = sqrt(stddev);

    focusData(i, 0) = space(i);
    focusData(i, 1) = stddev;
  }

  writeToCSVFile("out.csv", focusData);

  return 0;
}

