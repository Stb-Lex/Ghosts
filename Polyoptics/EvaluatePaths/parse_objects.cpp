#include"parse_objects.h"

#include<libxml/parser.h>
#include<libxml/tree.h>

#include<iostream>

std::vector<PolygonList> ParseObjectsXmlFile(const char* filename)
{
  std::vector<PolygonList> objects;
  
  LIBXML_TEST_VERSION
  xmlDoc* doc = xmlReadFile(filename, nullptr, 0);
  xmlNode* rootElem = xmlDocGetRootElement(doc);

  xmlNode* curNode = nullptr;
  for(curNode = rootElem->children; curNode; curNode = curNode->next)
  {
    if(curNode->type == XML_ELEMENT_NODE)
    {
      const xmlChar* curName = curNode->name;
      if(xmlStrcmp(curName, (const xmlChar*)"star") == 0)
      {
        double dx, dy, amplitude;
        unsigned int resolution;
        
        xmlAttr* attr = curNode->properties;

        if(xmlStrcmp(attr->name, (const xmlChar*)"dx") == 0)
          dx = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"dx\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"dy") == 0)
          dy = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"dy\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"amplitude") == 0)
          amplitude = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"amplitude\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"resolution") == 0)
          resolution = atoi((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"resolution\". Found: " << attr->name << std::endl;
          continue;
        }

        PolygonList star = BuildMegacamStarPolygon(dx, dy, 1., 1., resolution);
        star.SetOpacity(amplitude);
        objects.push_back(star);
      }
      else if(xmlStrcmp(curName, (const xmlChar*)"flat") == 0)
      {
        double dmax, radius;
        unsigned int resolution, sampleCount;

        xmlAttr* attr = curNode->properties;

        if(xmlStrcmp(attr->name, (const xmlChar*)"dmax") == 0)
          dmax = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"dmax\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"radius") == 0)
          radius = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"radius\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"resolution") == 0)
          resolution = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"resolution\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"pointcount") == 0)
          sampleCount = atoi((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"pointcount\". Found: " << attr->name << std::endl;
          continue;
        }

        PolygonList flat = BuildMegacamFlat(dmax, radius, resolution, sampleCount);
        objects.push_back(flat);
      }
      else if(xmlStrcmp(curName, (const xmlChar*)"round_star") == 0)
      {
        double dx, dy, amplitude;
        unsigned int resolution;
        
        xmlAttr* attr = curNode->properties;

        if(xmlStrcmp(attr->name, (const xmlChar*)"dx") == 0)
          dx = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"dx\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"dy") == 0)
          dy = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"dy\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"amplitude") == 0)
          amplitude = atof((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"amplitude\". Found: " << attr->name << std::endl;
          continue;
        }

        attr = attr->next;

        if(xmlStrcmp(attr->name, (const xmlChar*)"resolution") == 0)
          resolution = atoi((const char*)xmlNodeListGetString(curNode->doc, attr->children, 1));
        else
        {
          std::cout << "Could find element named \"resolution\". Found: " << attr->name << std::endl;
          continue;
        }

        PolygonList star = BuildStarPolygon(dx, dy, 1., 1., resolution);
        star.SetOpacity(amplitude);
        objects.push_back(star);
      }
      else
      {
        std::cout << "Unknown object: " << curName << std::endl;
      }

    }
  }
  
  xmlFreeDoc(doc);
  xmlCleanupParser();

  return objects;
}

