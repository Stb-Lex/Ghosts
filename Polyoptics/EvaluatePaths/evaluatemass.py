import subprocess
import sys

if(len(sys.argv) < 5):
    print("Evaluate an objects file multiple time (usefull for flats)")
    print("Uses multiprocessing")
    print("Usage: python3 evaluatemass.py evalCount outDirectoryAndPostfix objectsFilename polynomeFilename")
    exit()

evalCount = int(sys.argv[1])
out = sys.argv[2]
objectsFilename = sys.argv[3]
polynomeFilename = sys.argv[4]

for i in range(0, evalCount):
    subprocess.Popen(["./EvaluatePaths", objectsFilename, polynomeFilename, out + str(i)], stdout=open(out + "/log" + str(i), 'w'))


