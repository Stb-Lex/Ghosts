#ifndef PARSE_OBJECTS_H
#define PARSE_OBJECTS_H

#include<vector>
#include<Shared/source.h>

std::vector<PolygonList> ParseObjectsXmlFile(const char* filename);

#endif // PARSE_OBJECTS_H

