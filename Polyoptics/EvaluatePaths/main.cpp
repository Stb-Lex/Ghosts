#include<Shared/optical_path.h>
#include<Shared/source.h>

#include<chrono>
#include<fstream>
#include<iostream>
#include<string>

#include"parse_objects.h"


int main(int argc, char** argv)
{
  if(argc != 4)
  {
    std::cout << "Evaluate targets" << std::endl;
    std::cout << "Usage : EvaluatePaths objectsFilename compiledPathsFilename polygonFilenamePostfix" << std::endl;
    return 0;
  }

  const char* objectsFilename = argv[1];
  const char* compiledPathsFilename = argv[2];
  const char* polygonFilenamePostfix = argv[3];
  
  std::vector<OpticalPath> opticalPaths;
  PolygonList polygonList;

  cout << "Loading compiled paths " << compiledPathsFilename << " ..." << flush;
  ifstream is(compiledPathsFilename);
  if(is.is_open())
  {
    DeserializeOpticalPaths(is, opticalPaths);
    is.close();
  }
  cout << "Done" << endl;
  cout << opticalPaths.size() << " paths found" << endl;
  cout << "Using order " << opticalPaths[0].GetSystem().trunc_degree << " polynoms" << endl;
  
  cout << "Loading objects " << objectsFilename << " ...";

  std::vector<PolygonList> objects = ParseObjectsXmlFile(objectsFilename);
  
  cout << "Done" << endl;
  cout << "Found " << objects.size() << " objects" << endl;
  
  cout << "Evaluating polynoms..." << endl;
  std::chrono::system_clock::time_point begin = std::chrono::system_clock::now();
  
  for(unsigned int k = 0; k < objects.size(); k++)
  {
    for(unsigned int i = 0; i < opticalPaths.size(); i++)
    {
      PolygonList curPoly = opticalPaths[i].Evaluate(objects[k]);
      
      // Calculate mean jacobian (order 0) and standard deviation
      for(unsigned int j = 0; j < curPoly.GetSize(); j++)
      {
        double meanJacobian = 0.0;

        for(unsigned int k = 0; k < curPoly[j].points.size(); k++)
          meanJacobian += opticalPaths[i].GetJacobian(curPoly[j].points[k]);

        meanJacobian /= curPoly[j].points.size();

        //curPoly.SetOpacity(abs(opticalPaths[i].GetTransmission()/meanJacobian), j);
        curPoly.SetOpacity(opticalPaths[i].GetTransmission());
        polygonList.Append(curPoly[j]);
      }
    }
  }

  std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
  cout << "Done." << endl;
  cout << "Elapsed time: " << std::chrono::duration_cast<std::chrono::seconds>(end-begin).count() << "s" << endl;
  
  cout << "Saving polygons to csv..." << endl;
  polygonList.SaveToCsv(polygonFilenamePostfix);
  cout << "Done." << endl;

  return 0;
}

