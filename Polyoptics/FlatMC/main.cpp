#include<Shared/optical_path.h>
#include<Shared/source.h>
#include<Shared/phase_space.h>

#include<Eigen/Dense>
#include<Eigen/Core>

#include<chrono>
#include<fstream>
#include<iostream>
#include<string>


const static Eigen::IOFormat CSVFormat(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", "\n");

void writeToCSVFile(const char* name, const Eigen::MatrixXf& matrix)
{
  ofstream file(name);
  file << matrix.format(CSVFormat);
  file.close();
}


int main(int argc, char** argv)
{
  if(argc != 4)
  {
    std::cout << "Generate flats using polynomial optics. Outputs raw .csv file interpreted by draw_graph.py." << std::endl;
    std::cout << "Usage : FlatMC compiledPathsFilename sampleSize outFilename" << std::endl;
    return 0;
  }

  const char* compiledPathsFilename = argv[1];
  const char* outFilename = argv[3];
  unsigned int sampleSize = atoi(argv[2]);
  
  std::vector<OpticalPath> opticalPaths;
  
  cout << "Loading compiled paths " << compiledPathsFilename << " ..." << flush;
  ifstream is(compiledPathsFilename);
  if(is.is_open())
  {
    DeserializeOpticalPaths(is, opticalPaths);
    is.close();
  }
  cout << "Done" << endl;
  cout << opticalPaths.size() << " paths found" << endl;
  cout << "Using order " << opticalPaths[0].GetSystem().trunc_degree << " polynoms" << endl;

  cout << "Generating samples..." << endl;
  Eigen::MatrixXf samples = Eigen::MatrixXf::Random(4, sampleSize);

  float angleMax = 1.6406e-2;
  
  samples.row(2) = angleMax*samples.row(2);
  samples.row(3) = angleMax*samples.row(3);

  Eigen::MatrixXf outSamples = Eigen::MatrixXf::Zero(3, sampleSize*opticalPaths.size());

  cout << "Done" << endl;

  cout << "Evaluating flat..." << endl;
  std::chrono::system_clock::time_point begin = std::chrono::system_clock::now();

  for(unsigned int i = 0; i < opticalPaths.size(); i++)
  {
    for(unsigned int j = 0; j < sampleSize; j++)
    {
      PhaseSpacePoint in(samples(0, j), samples(1, j), samples(2, j), samples(3, j));
      PhaseSpacePoint out = opticalPaths[i].Evaluate(in);
      outSamples(0, i*sampleSize+j) = out.px;
      outSamples(1, i*sampleSize+j) = out.py;
      outSamples(2, i*sampleSize+j) = abs(opticalPaths[i].GetTransmission()/opticalPaths[i].GetJacobian(PhaseSpacePoint()));
    }
  }

  std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
  cout << "Done." << endl;
  cout << "Elapsed time: " << std::chrono::duration_cast<std::chrono::seconds>(end-begin).count() << "s" << endl;
  
  cout << "Saving samples to csv..." << endl;
  writeToCSVFile(outFilename, outSamples.transpose());
  cout << "Done." << endl;

  return 0;
}

