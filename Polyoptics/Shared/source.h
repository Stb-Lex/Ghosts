#ifndef SOURCE_H
#define SOURCE_H


#include"phase_space.h"

#include<vector>
#include<string>


struct Polygon
{
Polygon(void) : points(std::vector<PhaseSpacePoint>()), opacity(0) {}
Polygon(const std::vector<PhaseSpacePoint>& _points, double _opacity) : points(_points), opacity(_opacity) {}

  void Append(const Polygon& pol);
  
  std::vector<PhaseSpacePoint> points;
  double opacity;
};


class PolygonList
{
 public:
 PolygonList(void) {}
 PolygonList(const Polygon& pol) { m_polygons.push_back(pol); }
  
  void Append(const Polygon& polygon) { m_polygons.push_back(polygon); }
  void Append(const PolygonList& polygonList);

  void SetOpacity(double opacity);
  void SetOpacity(double opacity, unsigned int polygonIndex);

  void BuildRawPolygonArray(std::vector<double>& rawPolygons, std::vector<unsigned int>& polygonSection) const;

  Polygon operator[](unsigned int i) const { return m_polygons[i]; }

  unsigned int GetSize(void) const { return m_polygons.size(); }

  void SaveToCsv(const std::string& filename) const;
  
 private:
  std::vector<Polygon> m_polygons;
};


Polygon BuildStarPolygon(double dx, double dy, double sizex, double sizey, unsigned int resolution);
Polygon BuildMegacamStarRightTopEarPolygon(double dx, double dy, double sizex, double sizey, unsigned int resolution);
PolygonList BuildMegacamStarPolygon(double dx, double dy, double sizex, double sizey,unsigned int resolution);
PolygonList BuildMegacamFlat(double dmax, double radius, unsigned int resolution, unsigned int sampleCount);

#endif // SOURCE_H

