#include"layer.h"
#include<OpticalElements/Spherical5d.hh>

void Layer::Build(double lambda, const OpticalMaterial& backLayerMat, int trunc)
{
  if(!m_isMirror)
  {
    m_fromLeftTransmission = refract_spherical_5d(m_radius, backLayerMat.get_index(lambda), m_mat.get_index(lambda), trunc);
    m_fromRightTransmission = refract_spherical_5d(-m_radius, m_mat.get_index(lambda), backLayerMat.get_index(lambda), trunc);
    m_fromLeftReflexion = reflect_spherical_5d(m_radius, trunc);
    m_fromRightReflexion = reflect_spherical_5d(-m_radius, trunc);
  }
  else
  {
    m_fromLeftTransmission = reflect_spherical_5d(m_radius, trunc);
    m_fromRightTransmission = reflect_spherical_5d(m_radius, trunc);
    m_fromLeftReflexion = reflect_spherical_5d(m_radius, trunc);
    m_fromRightReflexion = reflect_spherical_5d(m_radius, trunc);
  }
}

