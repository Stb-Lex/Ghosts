#include"megacam.h"

LayerStack BuildMegacam(float dx, float lambda, int trunc)
{
  LayerStack megacam(lambda, trunc);

  OpticalMaterial air;

  megacam.SetFrontMirror(true);
  megacam.SetFrontAirLayerThickness(13.);
  megacam.SetFrontMirrorRadius(-27.0670);
  megacam.SetFrontMirrorAirThickness(11.4762);

  // L1
  megacam.PushLayer(Layer(OpticalMaterial(1.516728), 44.682e-3, MegacamTransmissionLens, 0.87694));
  megacam.PushLayer(Layer(air, 0.723218, MegacamTransmissionLens, 1.06491));

  // L2
  megacam.PushLayer(Layer(OpticalMaterial(1.516775), 29.725e-3, MegacamTransmissionLens, 1.99884));
  megacam.PushLayer(Layer(air, 0.303475, MegacamTransmissionLens, 0.65129));

  // L3
  megacam.PushLayer(Layer(OpticalMaterial(1.516803), 29.923e-3, MegacamTransmissionLens, 7.46712));
  megacam.PushLayer(Layer(air, 0.672977, MegacamTransmissionLens));

  // L4
  megacam.PushLayer(Layer(OpticalMaterial(1.516494), 40.016e-3, MegacamTransmissionLens, 1.06142));
  megacam.PushLayer(Layer(air, 0.037684, MegacamTransmissionLens));

  // ISU
  megacam.PushLayer(Layer(OpticalMaterial(1.458464), 10.000e-3, MegacamTransmissionLens));
  megacam.PushLayer(Layer(air, 0.24710 + dx, MegacamTransmissionLens));

  // Filter
  megacam.PushLayer(Layer(OpticalMaterial(1.516330), 5e-3, MegacamTransmissionFilter));
  megacam.PushLayer(Layer(air, 0.014500, MegacamTransmissionFilter));

  // Windows
  megacam.PushLayer(Layer(OpticalMaterial(1.516330), 25e-3, MegacamTransmissionLens));
  megacam.PushLayer(Layer(air, 0.027000, MegacamTransmissionLens));

  megacam.SetFocalPlanReflexion(MegacamReflectivityFocalPlan);

  megacam.BuildStack();

  return megacam;
}

