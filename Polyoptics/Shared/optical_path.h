#ifndef OPTICAL_PATH_H
#define OPTICAL_PATH_H


#include"phase_space.h"
#include"source.h"
#include<TruncPoly/TruncPolySystem.hh>
#include<string>


class OpticalPath
{
 public:
 OpticalPath(void) : m_poly(System44f()), m_T(0.0), m_path("") {}
 OpticalPath(const System44f& poly, double T, const std::string& path) : m_poly(poly), m_T(T), m_path(path) {}

  PhaseSpacePoint Evaluate(const PhaseSpacePoint& point);
  Polygon Evaluate(const Polygon& polygon);
  PolygonList Evaluate(const PolygonList& polygonList);

  double GetTransmission(void) const { return m_T; }
  double GetJacobian(const PhaseSpacePoint& point);
  std::string GetPath(void) const { return m_path; }
  System44f GetSystem(void) const { return m_poly; }

  friend ostream& operator<<(ostream& os, const OpticalPath& opticalPath);
  friend istream& operator>>(istream& is, OpticalPath& opticalPath);

 private:
  System44f m_poly;
  double m_T;
  string m_path;
};

ostream& SerializeOpticalPaths(ostream& os, const std::vector<OpticalPath>& opticalPaths);
istream& DeserializeOpticalPaths(istream& is, std::vector<OpticalPath>& opticalPaths);


inline PhaseSpacePoint OpticalPath::Evaluate(const PhaseSpacePoint& point)
{
  PhaseSpacePoint out;
  
  float rawIn[4];
  float rawOut[4];
  rawIn[0] = point.px;
  rawIn[1] = point.py;
  rawIn[2] = point.dx;
  rawIn[3] = point.dy;
  m_poly.evaluate(rawIn, rawOut);

  return PhaseSpacePoint(rawOut[0], rawOut[1], rawOut[2], rawOut[3]);
}


inline Polygon OpticalPath::Evaluate(const Polygon& polygon)
{
  Polygon out(std::vector<PhaseSpacePoint>(polygon.points.size()), polygon.opacity);

  for(unsigned int i = 0; i < polygon.points.size(); i++)
    out.points[i] = Evaluate(polygon.points[i]);

  return out;
}


inline PolygonList OpticalPath::Evaluate(const PolygonList& polygonList)
{
  PolygonList out;
  for(unsigned int i = 0; i < polygonList.GetSize(); i++)
  {
    Polygon outPolygon(std::vector<PhaseSpacePoint>(polygonList[i].points.size()), polygonList[i].opacity);
    for(unsigned int j = 0; j < polygonList[i].points.size(); j++)
      outPolygon.points[j] = Evaluate(polygonList[i].points.at(j));
    out.Append(outPolygon);
  }

  return out;
}

inline double OpticalPath::GetJacobian(const PhaseSpacePoint& point)
{
  float rawIn[4];
  rawIn[0] = point.px;
  rawIn[1] = point.py;
  rawIn[2] = point.dx;
  rawIn[3] = point.dy;

  return m_poly.get_jacobian_det(rawIn);
}


#endif // OPTICAL_PATH_H


