#ifndef LAYER_STACK_H
#define LAYER_STACK_H

#include<vector>
#include<cstdlib>
#include<string>
#include"layer.h"
#include"optical_path.h"

class LayerStack
{
 public:
 LayerStack(double lambda, int trunc = 5) : m_lambda(lambda), m_trunc(trunc) {}
  
  void PushLayer(const Layer& layer) { m_layerStack.push_back(layer); }

  void BuildStack(const OpticalMaterial& air = OpticalMaterial());

  OpticalPath ComputeOpticalPath(const std::string& path) const;

  void SetFrontMirror(bool frontMirror) { m_frontMirror = frontMirror; }
  void SetFrontAirLayerThickness(double thickness) { m_frontAirLayerThickness = thickness; }
  void SetFrontMirrorRadius(double radius) { m_frontMirrorRadius = radius; }
  void SetFrontMirrorAirThickness(double thickness) { m_frontMirrorAirThickness = thickness; }

  void SetFocalPlanReflexion(double R) { m_focalPlanR = R; }

 private:
  std::vector<Layer> m_layerStack;

  bool m_frontMirror;
  double m_frontAirLayerThickness;
  double m_frontMirrorRadius;
  double m_frontMirrorAirThickness;

  double m_lambda;

  int m_trunc;

  double m_focalPlanR;

};

#endif // LAYER_STACK_H

