#ifndef LAYER_H
#define LAYER_H

#include<limits>
#include<cstdlib>
#include<OpticalElements/OpticalMaterial.hh>
#include<TruncPoly/TruncPolySystem.hh>


class Layer
{
 public:
 Layer(const OpticalMaterial& mat, double thickness, double T, double radius = std::numeric_limits<double>::infinity(), bool isMirror = false) : m_mat(mat), m_thickness(thickness), m_T(T), m_radius(radius), m_isMirror(isMirror) {}

  void Build(double lambda, const OpticalMaterial& frontLayerMat, int trunc);

  Transform4d GetFromRightTransmission(void) const { return m_fromRightTransmission; }
  Transform4d GetFromLeftTransmission(void) const { return m_fromLeftTransmission; }
  Transform4d GetFromRightReflexion(void) const { return m_fromRightReflexion; }
  Transform4d GetFromLeftReflexion(void) const { return m_fromLeftReflexion; }

  double GetThickness(void) const { return m_thickness; }

  double GetRadius(void) const { return m_radius; }

  OpticalMaterial GetOpticalMaterial(void) const { return m_mat; }

  double GetTransmission(void) const { return m_T; }
  double GetReflexion(void) const { return ((m_isMirror) ? (m_T) : (1.0 - m_T)); }

  bool IsMirror(void) const { return m_isMirror; }

 protected:
  Transform4f m_fromRightTransmission;
  Transform4f m_fromLeftTransmission;
  Transform4f m_fromRightReflexion;
  Transform4f m_fromLeftReflexion;

 private:
  OpticalMaterial m_mat;
  double m_thickness;
  double m_T;
  double m_radius;
  bool m_isMirror;

};

#endif // LAYER_H

