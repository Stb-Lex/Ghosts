#ifndef MEGACAM_H
#define MEGACAM_H


#include"layer_stack.h"

const double MegacamTransmissionLens = 0.98;
const double MegacamTransmissionFilter = 0.8;
const double MegacamReflectivityFocalPlan = 0.3;


LayerStack BuildMegacam(float dx, float lambda, int trunc=5);


#endif // MEGACAM_H

