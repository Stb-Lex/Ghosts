#include"optical_path.h"

std::ostream& operator<<(std::ostream& os, const OpticalPath& opticalPath)
{
  System44f poly = opticalPath.GetSystem();
  double T = opticalPath.GetTransmission();
  std::string path = opticalPath.GetPath();
  std::size_t pathSize = path.size()*sizeof(char);
  
  os << poly;
  os.write(reinterpret_cast<const char*>(&pathSize), sizeof(std::size_t));
  os.write(path.c_str(), pathSize);
  os.write(reinterpret_cast<const char*>(&T), sizeof(double));

  return os;
}


std::istream& operator>>(std::istream& is, OpticalPath& opticalPath)
{
  System44f poly;
  double* T;
  std::string path;
  std::size_t* pathSize;
  char buffer[1024];

  is >> poly;

  is.read(buffer, sizeof(std::size_t));
  pathSize = static_cast<std::size_t*>(static_cast<void*>(buffer));

  size_t size = *pathSize;
  is.read(buffer, size);
  path = std::string(buffer, size);

  is.read(buffer, sizeof(double));
  T = static_cast<double*>(static_cast<void*>(buffer));

  opticalPath = OpticalPath(poly, *T, path);

  return is;
}


std::ostream& SerializeOpticalPaths(std::ostream& os, const std::vector<OpticalPath>& opticalPaths)
{
  std::size_t size = opticalPaths.size();
  
  os.write(reinterpret_cast<const char*>(&size), sizeof(std::size_t));
  
  for(unsigned int i = 0; i < size; i++)
    os << opticalPaths[i];
  
  return os;
}

std::istream& DeserializeOpticalPaths(std::istream& is, std::vector<OpticalPath>& opticalPaths)
{
  std::size_t* pSize;
  char buffer[sizeof(std::size_t)];

  is.read(buffer, sizeof(std::size_t));
  pSize = static_cast<std::size_t*>(static_cast<void*>(buffer));
  std::size_t size = *pSize;

  opticalPaths = std::vector<OpticalPath>(size);

  for(unsigned int i = 0; i < size; i++)
    is >> opticalPaths[i];

  return is;
}

