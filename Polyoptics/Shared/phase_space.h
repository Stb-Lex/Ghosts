#ifndef PHASE_SPACE_H
#define PHASE_SPACE_H

#include<iostream>

// Struct used to represent a reduced point in phase space
struct PhaseSpacePoint
{
PhaseSpacePoint(void) : px(0.f), py(0.f), dx(0.f), dy(0.f) {} // Degenerate AF
PhaseSpacePoint(float _px, float _py, float _dx, float _dy) : px(_px), py(_py), dx(_dx), dy(_dy) {}
  float px, py;
  float dx, dy;
};

inline std::ostream& operator<<(std::ostream& os, const PhaseSpacePoint& psp)
{
  os << "((" << psp.px << ", " << psp.py << "),(" << psp.dx << ", " << psp.dy << "))";
  return os;
}

#endif // PHASE_SPACE_H

