#include"source.h"
#include<cmath>
#include<fstream>
#include<string.h>
#include<Eigen/Dense>
#include<Eigen/Core>

const double pi = 3.141592653589793f; // Yes, we have to....
const double MegacamInnerRadius = 0.4f;
const double MegacamCrossWidth = 0.01f;

/*
void PolygonList::BuildRawPolygonArray(std::vector<double>& rawPolygons, std::vector<unsigned int>& polygonSections) const
{
  // First calculate the number of points
  unsigned int pointCount;
  for(unsigned int i = 0; i < m_polygons.size(); i++)
    pointCount += m_polygons[i].points.size();
  
  rawPolygons = std::vector<double>(pointCount*2);
  polygonSections = std::vector<unsigned int>(m_polygons.size());

  unsigned int curIndex = 0;
  for(unsigned int i = 0; i < m_polygons.size(); i++)
  {
    polygonSections[i] = m_polygons[i].size();
    for(unsigned int j = 0; j < m_polygons[i].size(); j++)
    {
      rawPolygons[curIndex+j] = m_polygons[i].at(j).px;
      rawPolygons[curIndex+j+pointCount] = m_polygons[i].at(j).py;
    }

    curIndex += m_polygons[i].size();
  }
}
*/

/*
void Polygon::Append(const Polygon& pol)
{
  std::vector<PhaseSpacePoint> newPoints(points.size() + pol.points.size());
  
  memcpy(&newPoints[0], &points[0], points.size());
  memcpy(&newPoints[points.size()], &pol.points[0], pol.points.size());

  points = newPoints;
}
*/

void PolygonList::Append(const PolygonList& polygonList)
{
  for(unsigned int i = 0; i < polygonList.GetSize(); i++)
    Append(polygonList[i]);
}


void PolygonList::SetOpacity(double opacity)
{
  for(unsigned int i = 0; i < m_polygons.size(); i++)
    m_polygons[i].opacity = opacity;
}


void PolygonList::SetOpacity(double opacity, unsigned int polygonIndex)
{
  m_polygons[polygonIndex].opacity = opacity;
}


void PolygonList::SaveToCsv(const std::string& filename) const
{
  std::string filenamePolygonPoints = filename + std::string("_points.csv");
  std::string filenamePolygonSections = filename + std::string("_sections.csv");
  std::ofstream filePolygonPoints(filenamePolygonPoints.c_str(), std::ofstream::out);
  std::ofstream filePolygonSections(filenamePolygonSections.c_str(), std::ofstream::out);

  for(unsigned int i = 0; i < m_polygons.size(); i++)
  {
    for(unsigned int j = 0; j < m_polygons[i].points.size(); j++)
      filePolygonPoints << m_polygons[i].points[j].px << "," << m_polygons[i].points[j].py << std::endl;

    filePolygonSections << m_polygons[i].points.size() << "," << m_polygons[i].opacity << std::endl;
  }

  filePolygonPoints.close();
  filePolygonSections.close();
}


Polygon BuildStarPolygon(double dx, double dy, double sizex, double sizey, unsigned int resolution)
{
  double dtheta = 2*pi/(double)resolution;

  Polygon pol(std::vector<PhaseSpacePoint>(resolution), 1.0);

  for(unsigned int i = 0; i < resolution; i++)
  {
    pol.points[i].px = sizex/2.f*cos(dtheta*(double)i);
    pol.points[i].py = sizey/2.f*sin(dtheta*(double)i);
    pol.points[i].dx = dx;
    pol.points[i].dy = dy;
  }
  
  return pol;
}


Polygon BuildMegacamStarRightTopEarPolygon(double dx, double dy, double sizex, double sizey, unsigned int resolution)
{
  Polygon pol(std::vector<PhaseSpacePoint>(2*resolution), 1.0);

  pol.points[0].px = sizex/2.f*MegacamInnerRadius;
  pol.points[0].py = sizey/2.f*MegacamCrossWidth;
  pol.points[2*resolution-1].px = sizex/2.f;
  pol.points[2*resolution-1].py = sizey/2.f*MegacamCrossWidth;

  pol.points[0].dx = dx;
  pol.points[0].dy = dy;
  pol.points[2*resolution-1].dx = dx;
  pol.points[2*resolution-1].dy = dy;

  for(unsigned int i = 1; i < resolution; i++)
  {
    pol.points[i].px = cos(pi/2.f/resolution)*pol.points[i-1].px - sin(pi/2.f/resolution)*pol.points[i-1].py;
    pol.points[i].py = sin(pi/2.f/resolution)*pol.points[i-1].px + cos(pi/2.f/resolution)*pol.points[i-1].py;
    pol.points[2*resolution-i-1].px = cos(pi/2.f/resolution)*pol.points[2*resolution-i].px - sin(pi/2.f/resolution)*pol.points[2*resolution-i].py;
    pol.points[2*resolution-i-1].py = sin(pi/2.f/resolution)*pol.points[2*resolution-i].px + cos(pi/2.f/resolution)*pol.points[2*resolution-i].py;

    pol.points[i].dx = dx;
    pol.points[i].dy = dy;
    pol.points[2*resolution-i-1].dx = dx;
    pol.points[2*resolution-i-1].dy = dy;
  }

  return pol;
}


PolygonList BuildMegacamStarPolygon(double dx, double dy, double sizex, double sizey, unsigned int resolution)
{
  Polygon ear = BuildMegacamStarRightTopEarPolygon(dx, dy, sizex, sizey, resolution);
  PolygonList finalPolygonList;

  for(unsigned int i = 0; i < 4; i++)
  {
    Polygon curEar(ear);
    
    for(unsigned int j = 0; j < ear.points.size(); j++)
    {
      curEar.points[j].px = cos(i*pi/2.f)*ear.points[j].px - sin(i*pi/2.f)*ear.points[j].py;
      curEar.points[j].py = sin(i*pi/2.f)*ear.points[j].px + cos(i*pi/2.f)*ear.points[j].py;
    }
    
    finalPolygonList.Append(curEar);
  }


  return finalPolygonList;
}

PolygonList BuildMegacamFlat(double dmax, double radius, unsigned int resolution, unsigned int sampleCount)
{
  Eigen::MatrixXd samplePosition = Eigen::MatrixXd::Random(2, sampleCount);
  Eigen::MatrixXd sampleAngles = Eigen:: MatrixXd::Random(2, sampleCount);

  samplePosition = 2.0*samplePosition + Eigen::MatrixXd::Ones(2, sampleCount);
  sampleAngles = dmax*(2.0*sampleAngles + Eigen::MatrixXd::Ones(2, sampleCount));
  
  PolygonList polygonList;

  for(unsigned int i = 0; i < sampleCount; i++)
    polygonList.Append(BuildStarPolygon(sampleAngles(0, i), sampleAngles(1, i), radius, radius, resolution));

  return polygonList;
}

