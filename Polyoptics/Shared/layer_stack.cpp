#include"layer_stack.h"
#include<OpticalElements/Spherical5d.hh>
#include<OpticalElements/Propagation5d.hh>

void LayerStack::BuildStack(const OpticalMaterial& air)
{
  if(m_layerStack.size() == 0)
    return;

  // The mirror "layer"
  if(m_frontMirror)
  {
    Layer mirrorLayer(air, m_frontMirrorAirThickness, 1.0, m_frontMirrorRadius, true);
    mirrorLayer.Build(m_lambda, OpticalMaterial(), m_trunc);
    m_layerStack.insert(m_layerStack.begin(), mirrorLayer);
  }
  else
  {
    // if there is no front mirror, we init the first layer
    m_layerStack[0].Build(m_lambda, air, m_trunc);
  }

  for(unsigned int i = 1; i < m_layerStack.size(); i++)
    m_layerStack[i].Build(m_lambda, m_layerStack[i-1].GetOpticalMaterial(), m_trunc);

  // Focal plan "layer"
  Layer focalPlanLayer(air, 0, 1.f - m_focalPlanR);
  focalPlanLayer.Build(m_lambda, m_layerStack[m_layerStack.size()-1].GetOpticalMaterial(), m_trunc);
  m_layerStack.push_back(focalPlanLayer);
}


OpticalPath LayerStack::ComputeOpticalPath(const std::string& path) const
{
  // Propagation in free space before hitting any refractors/reflectors
  System44d poly = propagate_5d(m_frontAirLayerThickness, m_trunc);

  // Transmission through the first layer (in Megacam : reflexion on the main mirror)
  poly = poly >> m_layerStack[0].GetFromLeftTransmission() >> propagate_5d(m_layerStack[0].GetThickness(), 1);
  
  unsigned int curLayer = 1;
  double T = m_layerStack[0].GetTransmission();

  for(unsigned int i = 1; i < path.size(); i++)
  {
    if(path[i-1] == 'r')
    {
      if(path[i] == 'r')
      {
        // Right then right (transmission from the left)
        poly = poly >> m_layerStack[curLayer].GetFromLeftTransmission() >> propagate_5d(m_layerStack[curLayer].GetThickness(), m_trunc);
        T *= m_layerStack[curLayer].GetTransmission();
        curLayer++;
      }
      else
      {
        // Right then left (reflexion from the left)
        poly = poly >> m_layerStack[curLayer].GetFromLeftReflexion() >> propagate_5d(m_layerStack[curLayer-1].GetThickness(), m_trunc);
        T *= m_layerStack[curLayer].GetReflexion();
        curLayer--;
      }
    }
    else
    {
      if(path[i] == 'r')
      {
        // Left then right (reflexion from the right)
        poly = poly >> m_layerStack[curLayer].GetFromRightReflexion() >> propagate_5d(m_layerStack[curLayer].GetThickness(), m_trunc);
        T *= m_layerStack[curLayer].GetReflexion();
        curLayer++;
      }
      else
      {
        // Left then left (transmission from the right)
        poly = poly >> m_layerStack[curLayer].GetFromRightTransmission() >> propagate_5d(m_layerStack[curLayer-1].GetThickness(), m_trunc);
        T *= m_layerStack[curLayer].GetTransmission();
        curLayer--;
      }
    }
  }

  T *= (1 - m_focalPlanR);
  
  return OpticalPath(poly, T, path);
}

