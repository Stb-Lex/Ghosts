#include<Shared/layer_stack.h>
#include<Shared/megacam.h>
#include<Shared/source.h>
#include<fstream>
#include<vector>
#include<string>
#include<chrono>
#include<thread>
#include<mutex>

using namespace std;
using namespace std::chrono;

static std::mutex barrier;

void PrintPolygon(const Polygon& pol)
{
  for(unsigned int i = 0; i < pol.points.size(); i++)
    cout << pol.points[i] << endl;
}


void ComputePolynomial(const std::string& path, const LayerStack& layerStack, std::vector<OpticalPath>& opticalPaths, int& done)
{
  OpticalPath opticalPath = layerStack.ComputeOpticalPath(path);
  done = 1;
  
  std::lock_guard<std::mutex> block_threads_until_finish_this_job(barrier);
  opticalPaths.push_back(opticalPath);
}


int main(int argc, char** argv)
{
  if(argc < 6)
  {
    cout << "Usage : ComputePath pathFilename numThreads trunc dx compiledPathsFilename" << endl;
    return 0;
  }
  unsigned int numThreads = atoi(argv[2]);
  unsigned int trunc = atoi(argv[3]);
  double dx = (double)atof(argv[4]);
  std::string compiledPathsFilename(argv[5]);

  // float dx = -0.00800801f;
  //float dx = -0.008;
  
  LayerStack telescope = BuildMegacam(dx, 550., trunc);

  std::vector<OpticalPath> opticalPaths;

  // First load all paths
  cout << "Loading optical paths..." << endl;
  std::vector<std::string> paths;
  std::string line;
  std::ifstream pathsFile(argv[1]);
  if(pathsFile.is_open())
    while(getline(pathsFile, line))
      paths.push_back(line);
  else
  {
    cerr << "Could not load file: " << argv[1] << endl;
    return -1;
  }
  pathsFile.close();
  cout << "Done. " << paths.size() << " paths found." << endl;

  unsigned int polynomeCount = paths.size();
  //unsigned int polynomeCount = 1;

  // Initialising multithreading stuff
  unsigned int curPath = 0;
  std::vector<std::thread> threads(numThreads);
  std::vector<int> threadDone(numThreads); // true : job done, false : job running
  
  for(unsigned int i = 0; i < numThreads; i++)
  {
    threads[i] = std::thread();
    threadDone[i] = 1;
  }
  
  bool running = true;
  
  cout << "Computing polynoms..." << endl;
  
  std::chrono::system_clock::time_point begin = std::chrono::system_clock::now();
  while(running)
  {
    for(unsigned int i = 0; i < numThreads; i++)
    {
      if(threadDone[i] == 1)
      {
        if(threads[i].joinable())
          threads[i].join();
        threadDone[i] = 0;
        threads[i] = std::thread(ComputePolynomial, std::cref(paths[curPath]), std::cref(telescope), std::ref(opticalPaths), std::ref(threadDone[i]));
        curPath++;
        if(curPath >= polynomeCount)
        {
          running = false;
          break;
        }
      }
    }
    float progress = 100.f*curPath/polynomeCount;
    cout << "\b\b\b\b" << floor(progress) << "%" << flush;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  cout << endl;

  // Wait all threads finishs
  for(unsigned int i = 0; i < numThreads; i++)
  {
    while(!threads[i].joinable());
    threads[i].join();
  }

  std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
  
  cout << "Done." << endl;
  cout << "Elapsed time: " << std::chrono::duration_cast<std::chrono::seconds>(end-begin).count() << "s" << endl;

  cout << "Serializing polynoms..." << flush;
  begin = std::chrono::system_clock::now();
  ofstream os(compiledPathsFilename);
  if(os.is_open())
  {
    SerializeOpticalPaths(os, opticalPaths);
    os.close();
  }
  end = std::chrono::system_clock::now();
  cout << "Done." << endl;
  cout << "Elapsed time: " << std::chrono::duration_cast<std::chrono::seconds>(end-begin).count() << "s" << endl;
 
  return 0;
}
