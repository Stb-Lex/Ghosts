cmake_minimum_required(VERSION 2.8)

#set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS "-O3")

SET( EIGEN3_INCLUDE_DIR "$ENV{EIGEN3_INCLUDE_DIR}" )
IF( NOT EIGEN3_INCLUDE_DIR )
    MESSAGE( FATAL_ERROR "Please point the environment variable EIGEN3_INCLUDE_DIR to the include directory of your Eigen3 installation.")
ENDIF()
INCLUDE_DIRECTORIES ( "${EIGEN3_INCLUDE_DIR}" )

project(CompilePaths)

include_directories("../")

add_executable(CompilePaths main.cpp ../Shared/layer_stack.cpp ../Shared/megacam.cpp ../Shared/source.cpp ../Shared/optical_path.cpp ../Shared/layer.cpp)

set(CMAKE_CXX_FLAGS "-std=c++11 -pthread -Wall")

target_include_directories(CompilePaths PUBLIC "../")

