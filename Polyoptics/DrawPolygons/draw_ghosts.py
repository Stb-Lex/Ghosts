import numpy as np
import sys
import matplotlib.pyplot as plt
from astropy.io import fits
sys.path.insert(0, '../Rasterize/')

import rasterize_module as rast

filename = sys.argv[1]
outFilename = sys.argv[2]

print("Loading points...")
rpoints = np.loadtxt(open(filename+"_points.csv", "rb"), delimiter=",").T
print("Done.")
print("Loading sections...")
rsections = np.abs(np.loadtxt(open(filename+"_sections.csv", "rb"), delimiter=","))
print("Done.")

print("Drawing " + str(len(rsections)) + " polygons using GPU")

points = np.ascontiguousarray(rpoints, dtype=np.float32)
sections = np.ascontiguousarray(rsections[:,0], dtype=np.uint16)
opacity = np.ascontiguousarray(rsections[:,1], dtype=np.double)

resx = 512
resy = 512
#dimx = 0.14
#dimy = 0.14
dimx = 2
dimy = 2

#points[0] = (points[0]/dimx*resx+resx)/2
#points[1] = (-points[1]/dimy*resy+resy)/2

#dim = 0.14
dim = 2

translate = np.array([[dim/2], [-dim/2]], dtype=np.float32)
scale = np.array([[resx/dim], [-resy/dim]], dtype=np.float32)

points = (points+translate)*scale


ccdMat = np.zeros([resx, resy])

rast.RasterizePolygonBatchGPUPy(points, sections, opacity, ccdMat)

hdu = fits.PrimaryHDU(ccdMat)
hdulist = fits.HDUList([hdu])
hdulist.writeto(outFilename)

