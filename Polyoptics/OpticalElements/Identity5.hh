#ifndef identity5_hh
#define identity5_hh

#include"../TruncPoly/TruncPolySystem.hh"

inline void identity_5(PT4fData **polynomials, int *num_terms);


#ifndef _make_tpt4f_
#define _make_tpt4f_

inline const Transform4f make_TPT4f(PT4fData **polynomials, int *num_terms, int trunc = 5) {
  Transform4f pt;
  pt.trunc_degree = trunc;
  for (int i = 0; i < 4; ++i) {
    Poly4f poly(num_terms[i],polynomials[i],sizeof(PT4fData));
    pt[i] = poly; 
    pt[i].trunc_degree = trunc;
  }
  pt.consolidate_terms();
  return pt;
}

inline const Poly4f make_TP4f(PT4fData *polynomial, int num_terms, int trunc = 5) {
  Poly4f poly(num_terms,polynomial,sizeof(PT4fData));
  poly.trunc_degree = trunc;
  poly.consolidate_terms();
  return poly;
}

#endif

inline const Transform4f identity_5()
{
  int num_terms[4];
  PT4fData *element[4];
  for (int i = 0; i < 4; ++i) {
    element[i] = new PT4fData[1024];
  }
  identity_5(element, num_terms);
  Transform4f result = make_TPT4f(element, num_terms, 1);
  for (int i = 0; i < 4; ++i) {
    delete[] element[i];
  }
  return result;
}


inline void identity_5(PT4fData **polynomials, int *num_terms)
{
  num_terms[0] = 1;
  num_terms[1] = 1;
  num_terms[2] = 1;
  num_terms[3] = 1;
  
  polynomials[0][0] = PT4fData(1, 1, 0, 0, 0);
  polynomials[1][0] = PT4fData(1, 0, 1, 0, 0);
  polynomials[2][0] = PT4fData(1, 0, 0, 1, 0);
  polynomials[3][0] = PT4fData(1, 0, 0, 0, 1);
}


#endif // identity5_hh

